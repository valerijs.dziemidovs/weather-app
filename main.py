from flask import Flask
import requests
import json

app = Flask(__name__)

@app.route('/current')
def test():
    response = requests.get('http://api.weatherstack.com/current?access_key=5a022d932b8c8beaf454447d23ad503a&query=Miami')
    weather_info = response.json()
    return {'Weather': weather_info['current']}
